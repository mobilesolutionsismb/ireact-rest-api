### Get layers filtered by area of interest, start, end
GET {{host}}/api/services/app/Maps/v2/GetLayers
	?areaOfInterest=POLYGON%20%28%28-26.19140625%2033.87041555094183%2C%2042.01171875%2033.87041555094183%2C%2042.01171875%2071.52490903732816%2C%20-26.19140625%2071.52490903732816%2C%20-26.19140625%2033.87041555094183%29%29
	&startTime=2017-12-08
	&endTime=2018-03-30
	&livemode=true
Authorization: {{token}}
	
### Get layers filtered by area of interest, start, end, ireact task values
GET {{host}}/api/services/app/Maps/v2/GetLayers
	?areaOfInterest=POLYGON%20%28%28-26.19140625%2033.87041555094183%2C%2042.01171875%2033.87041555094183%2C%2042.01171875%2071.52490903732816%2C%20-26.19140625%2071.52490903732816%2C%20-26.19140625%2033.87041555094183%29%29
	&startTime=2017-12-08
	&endTime=2018-01-10
	&FilterIReactTasks=3203
	&FilterIReactTasks=3204
	&FilterIReactTasks=3205
Authorization: {{token}}

### Get layer metadata info (by metadata id)
GET {{host}}/api/services/app/Maps/GetLayerInfo
	?id=135687
Authorization: {{token}}

### Create a layers reimporting request
POST {{host}}/api/services/app/Maps/CreateLayersReimportingRequest
Content-Type: application/json
Authorization: {{token}}

{
  "start": "2017-06-08T14:18:53",
  "end": "2017-06-08T14:18:53"
}

### Get a layers reimporting request by id ###
GET {{host}}/api/services/app/Maps/GetLayersReimportingRequest?id=1
Authorization: {{token}}

### Get all layers reimporting requests ###
GET {{host}}/api/services/app/Maps/GetLayersReimportingRequestList
  ?datestart=2018-02-15T11:00:53
Authorization: {{token}}

### Delete a layers reimporting request by id ###
DELETE {{host}}/api/services/app/Maps/DeleteLayersReimportingRequest?id=1214
Authorization: {{token}}