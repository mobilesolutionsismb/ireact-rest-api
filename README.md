This repo contains files that allows to call REST API exposed by IREACT backend using [REST Client Extension for Visual Studio Code](https://github.com/Huachao/vscode-restclient).  

In order to use them:  

1. Install [Visual Studio Code](https://code.visualstudio.com/) (VS Code)  
2. Install [REST Client Extension for VS Code](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
3. Rename settings.json.example in settings.json 
4. Select environment (bottom bar on the right) 
5. Open *.rest* files in VS Code
6. Click on Send Request above the API you are interested in

# Environments
3 environments have been configured in settings.json.example:

* dev
* test
* production

## Change environment
To change the environment you just need to open Command Palette and use Rest Client: Switch Environment


# Authentication
1. Use [Authentication.rest](./Authentication.rest) and send request to POST {{host}}/api/TokenAuth/Authenticate
2. Get the token and copy it into [settings.json](./.vscode/settings.json) in the token field of the environment you're using

# Notes
Never push this repository becuase it is maintained by ISMB