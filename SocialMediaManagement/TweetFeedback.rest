### Add or update a tweet feedback
POST {{host}}/api/services/app/SocialMedia/CreateOrUpdateTweetFeedback
Content-Type: application/json
Authorization: {{token}}

{
  "tweetId": "1004000004447535105",
  "userId": 2,
  "properties": {
    key1: 3,
    key2: "value2",
    key3: ["value3", "othervalue3"]
  },
  "AddedClasses": ["Class_1", "Class_2", "Class_3"],
  "RemovedClasses": ["floods"],
  "InformativeNewValue": true,
  "SentimentNewValue":"none",
  "RemovedEntities":[{"id": "333", "iri": "my/iri/333", "label": "Entity_1"},{"id": "444", "iri": "my/iri/444", "label": "Entity_2"}],
  "AddedEntities": [{"id": "111", "iri": "my/iri/111", "label": "label_abc123"},{"id": "222", "iri": "my/iri/222", "label": "label_def345"}]
}

### Get tweet feedback list eventually filtered by userId, tweetId and lastModificationTime
GET {{host}}/api/services/app/SocialMedia/GetTweetFeedbackList
Authorization: {{token}}